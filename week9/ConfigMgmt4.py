#! /usr/bin/python3
# Ike-062421: Initial version.

import salt.client as salt
import math
import pymysql

''' 
    Simple script for determining some basic details of salt minions the code is executed on and then sending that information
    to a database.
'''




# Parameters for database
# host: localhost
# user: root
# password: Password1
# database: cmdb


def upload(items):
    ''' Simple method for uploading a list to a database..'''
    db = pymysql.connect(
            host='localhost',
            user='root',
            password='Password1',
            db='cmdb',
            charset='utf8mb4'
        )
    # Initialize a cursor object to execute database queries.
    cursor = db.cursor()
    statement = 'INSERT INTO device VALUES (' + str(items)[1:-1] + ');'
    # Attempt to query the database and make the changes.
    try:
        print(cursor.execute(statement), 'lines affected.')
        db.commit()
    # The attempt failed, ensure no changes and let the user know.
    except:
        db.rollback()
        print('Failed.')
    # Close the connection.
    db.close()


def main():
    '''Main method for querying SaltStack minions to update a database.'''
    # Instantiate a query object.
    local = salt.LocalClient()
    # Query to get a list of facts about the clients.
    full = local.cmd('*', 'grains.items')    
    
    # Iterate over all clients.
    for item in full.keys():
        # Build a list to hold the facts about the clients.
        export = []
        export.append(local.cmd(item, 'network.get_fqdn')[item])
        
        interfaces = local.cmd(item, 'network.interfaces')[item]
        
        interfaces_l = list(interfaces.keys())
        
        # Branched path: Figure out if client is Windows or Linux.
        if full[item]['os'] == 'Windows':

            export.append(interfaces[interfaces_l[0]]['hwaddr'])
        
        else:
            export.append(interfaces[interfaces_l[1]]['hwaddr'])
        
        export.append(local.cmd(item, 'network.ip_addrs')[item][0])
            
            
        export.append(full[item]['num_cpus'])
        if full[item]['os'] == 'Windows':
            export.append(len(local.cmd(item, 'disk.usage')[item].keys()))
        else:
            export.append(math.ceil(len(local.cmd(item, 'disk.blkid')[item]) / 8))
        export.append(math.ceil(full[item]['mem_total'] / 1024))
        export.append(full[item]['os']) 
        export.append(full[item]['osrelease'])
        # Let the user know what was exported to the database.
        print(export)
        # Export to the database.
        upload(export)


if __name__ == '__main__':
    main()
