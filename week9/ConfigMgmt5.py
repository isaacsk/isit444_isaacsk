#!/usr/bin/python3

# Ike-06232021: Initial version.

'''
Small script for populating Salt minions with relevant user information from the console.
Includes some basic handlers for ensuring proper input.
'''

# Import necessary modules.
import csv
import sys
import salt.client
import salt.modules


# Regulated values in the form of dictionaries.

district_dictionary = {
    'IHS',
    'NJH',
    'AH',
    'BHS',
    'BC'
}

rank_dictionary = {
    'staff',
    'student'
}


def usage():
    '''Method to remind the user how to use the program with expected results.'''
    print(
        'Please use this program in the following way:\n'
        './user1.py\n'
        'without any additional arguments or switches. The\n'
        'program will take you through the user creation process.\n'
        'On completion, please look for the\n'
        'profile.csv\n'
        'file that contains your generated users.'
    )
    return

def get_string_input(phrase, expected_dictionary):
    '''Ensure that given value exists within a dictionary.'''
    while(True):
        # Get the user-provided input.
        value = input(phrase)
        # Check if value exists within the dictionary.
        if value in expected_dictionary:
            return value
        # If it does not, remind the user what values are permitted.
        else:
            print(
                'The permitted values for this section are:',
            )
            for item in expected_dictionary:
                print(
                    '\t',
                    item
                )
    return

def generate_user_id(profile):
    ''' Generates the userid for the profile.'''
    
    # Determine if the profile is a staff or a student.
    if profile[3] == 'staff':
        # Generate a starting profile base (firstnamelastname)
        user_id = (profile[0][0] + profile[1]).lower()
        
        # Attempt to grab the last number of the account to increment.
        try:
            # Grab the last character of the existing user_id and then add one.
            user_id = user_id[:-1] + str(int(user_id[-1:]) + 1)
        
        except:
            # The last character is not a number, we append a 2 so that there is now a number.
            user_id = user_id + '2'
        
    # If not a staff account, then it must be a student account.
    else:
        # Generate the base userid.
        user_id = (profile[1][:5] + profile[0][:3]).lower()
        count = 0
        
        # Append the number to the userid.
        user_id = user_id + '{0:02d}'.format(count)
            
    # Pass the userid back to the calling function.
    return user_id



def build_profile():
    '''Constructs a profile to be written to a .csv format.'''
    profile = []
    # Get the type of account for the individual.
    profile.append(
        get_string_input(
            'Please enter the type of account for this individual (i.e. student or staff): ',
            rank_dictionary
        )
    )
    # Get the district associated with the individual.
    profile.append(
        get_string_input(
            'Please enter the district associated with the individual (press Enter for a complete list): ',
            district_dictionary
        )
    )
    # Get the last name of the individual. 
    name = input(
            'Please enter the last name of the individual: '
        )
    name.strip()
    name.lower()
    profile.append(name)

    # Get the first name of the individual.
    name = input(
            'Please enter the first name of the individual: '
        )
    name.strip()
    name.lower()
    profile.append(name)


    # Reverses profile list so that it fits required convention of
    # firstname, lastname, district, rank
    profile.reverse()
    # Generate the userid for the individual.
    profile.append(generate_user_id(profile))

    # Write the profile to the record file.
    #write_to_file(profile)
    return profile

def send_profile(arguments):
    '''
    Simple method for sending a built profile to the Salt minions.
    '''
    
    # Create a Salt query object.
    local = salt.client.LocalClient()
    
    # Query the objects and get their operating system identifier.
    full = local.cmd('*', 'grains.item', ['os'])
    
    # Iterate over the Salt minions.
    for item in full.keys():
        # Check to see if the user already exists within the computers.
        if arguments[4] in local.cmd('*', 'user.list_users').keys():
            # Notify the query-er that the user already exists.
            print('User already exists.')
            return

        if full[item]['os'] == 'Windows':
            # Build command.
            command = [arguments[4].replace(' ', ''), 'password=Password1', 'fullname=' + arguments[0].capitalize() + ' ' + arguments[1].capitalize(), 'groups=' + arguments[3]]
            # Send command.
            local.cmd(item, 'user.add', command)
            
        else:
            # Minion is Linux, so use linux commands.
            # Build command.
            command = [arguments[4].replace(' ', ''), 'gid=' + arguments[3], 'fullname=' + arguments[0].capitalize() + ' ' + arguments[1].capitalize()]
            # Send command.
            local.cmd(item, 'user.add', command)
            # This seems to work, but very poorly: assigns password of Password1 to user.
            local.cmd(item, 'cmd.run', ['echo ' + arguments[4].replace(' ', '') + ':Password1 | chpasswd'])


def main():
    '''Basic structure of program: new file, instantiate profile-writing loop.'''
    #with open('profile.csv', 'w') as file:
    #    writer = csv.writer(file)
    #    writer.writerow(['firstname', 'lastname', 'school', 'accounttype', 'userid'])
    
    loop = 'y'
    # Begin loop for adding individuals.
    while(loop == 'y'):
        send_profile(build_profile())
        loop = input('Would you like to add another individual (y/*)?\t')
    return


if __name__ == 'main':
    main()

# Ensure that no additional arguments are intentionally passed
# when calling the program.
if len(sys.argv) == 1:
    main()
else:
    usage()
