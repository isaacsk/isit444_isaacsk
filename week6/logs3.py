#!/usr/bin/python3

# Ike-20210520: Initial version.

import re
import csv
import requests
import time

def scan_file(filename):
    '''
    Method to scan two separate files: one for a list of I.P. addresses to check and a separate one for those same I.P. addresses
    and their MAC addresses. Then determines the registered vendor for those MAC addresses.
    '''
    
    # Initialize the lists and dictionaries for this method.
    mac_ip_dictionary = {}
    ip_searchlist = []
    search_mac_list = []
    bindings_list = []
    
    # Open file with given I.P. addresses.
    with open('input', 'r') as file:
        for line in file:
            # Regex query to look for I.P. addresses in the file.
            phrase = re.search('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', line)
            
            # If there are I.P. addresses present, adds them to the search list.
            if phrase:
                match = str(phrase).split()[4].split('\'')[1]
                
                ip_searchlist.append(match)

    # Open the file name to search for MAC addresses within once the I.P. addresses
    # have been determined.
    with open(filename, 'r') as file:
        for line in file:
            # Regex query to look for the matching I.P. address and MAC address.
            phrase = re.search('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} (from|to) []{1,2}:[0-9a-fA-F]+', line)

            if phrase:
                match = str(phrase).split()
                mac_and_ip = match[6][:-2] + '-' + match[4][7:]
                
                marker = mac_and_ip.split('-')
                # If the phrase is found, add it to the respective lists and dictionaries to query later.
                if marker[1] in ip_searchlist and marker not in bindings_list:
                    bindings_list.append(marker)
                    mac_ip_dictionary[marker[0]] = marker[1]
                    if marker[1] not in search_mac_list:
                        search_mac_list.append(marker[0])

    # Open a new .csv file to write the results to.
    with open ('mac_search.csv', 'w') as file:            
        writer = csv.writer(file)
        # Write headers for the columns.
        writer.writerow(['IP', 'MAC Address', 'Vendor'])
        # Iterate over the searching MAC addresses and query the API for them.
        for item in search_mac_list:
            phrase = 'https://api.macvendors.com/' + item
            api_call = requests.get(phrase)
            # Print out the device and the return.
            print(mac_ip_dictionary[item], ':', api_call.text.replace(',',''))
            # Add the item to the .csv file.
            writer.writerow([mac_ip_dictionary[item], item, api_call.text.replace(',','')])
            # Wait one second between each query so that the API handler does not throttle the connection.
            time.sleep(1)
    
    return


def main():
    scan_file('dhcpdsmall.log')
    return

if __name__ == '__main__':
    main()
