#!/usr/bin/python3

# Ike-20210520: Initial version.

import re
import csv



def scan_file(filename):
    ''' Short method for writing the number of ACKs of a given device to a .csv record.'''
    # Initialize dictionary.
    mac_ip_dictionary = {}
    # Open the file to search.
    with open(filename, 'r') as file:
        
        # Iterate over the file.
        for line in file:
            # Regex query for searching for a DHCP ACK with an IP and MAC address on the same line.
            phrase = re.search('DHCPACK (for|on) [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} (from|to) []{1,2}:[0-9a-fA-F]+', line)
            
            # If the query is found, split and record it.
            if phrase:
                match = str(phrase).split()

                mac_and_ip = match[8][:-2] + '-' + match[6]
                if mac_and_ip not in mac_ip_dictionary.keys():
                    mac_ip_dictionary[mac_and_ip] = 1
                else:
                    mac_ip_dictionary[mac_and_ip] = mac_ip_dictionary[mac_and_ip] + 1
    
    # Sort the list to make finding the highest ACK values easier to find.
    sorted_list = sorted(mac_ip_dictionary.values())
    
    # Iterate over the dictionary and find and print the values with the highest ACK values.
    for item in mac_ip_dictionary:
        if mac_ip_dictionary[item] in sorted_list[-2:]:
            print(item.split('-')[0] + ':', str(mac_ip_dictionary[item]))
    
    # Write the record of all device ACKs to a new .csv file.
    with open('record.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['Mac address', 'IP address', 'Total number of acks'])
        for item in mac_ip_dictionary:
            broken = item.split('-')
            writer.writerow([broken[0], broken[1], mac_ip_dictionary[item]])
            
    return


def main():
    return

if '__name__' == 'main':
    main()


scan_file('dhcpdsmall.log')
