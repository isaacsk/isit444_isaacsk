#!/usr/bin/python3

# Ike-20210519: Initial version.

import re




def scan_file(filename):
    '''Scans a givn file for any MAC addresses that have the phrase "(iPhone)" on them.'''
    # Initialize dictionary.
    mac_dictionary = {}
    # Open the requisite file.
    with open(filename, 'r') as file:
        
        # Loop iterating over the file.
        for line in file:
            # Regex search for the phrase "from" or "to" and then a MAC address with (iPhone) after.
            search = re.compile('(from|to) []{1,2}:[0-9a-fA-F]+..+iphone', re.IGNORECASE)
            phrase = re.search(search, line)
            # If the phrase is found, add it to the dictionary if it has not been seen before.
            if phrase:
                match = str(phrase).split()
                # Check to see if the device has been seen before.
                if match[5] in mac_dictionary.keys():
                    mac_dictionary[match[5]] = mac_dictionary[match[5]] + 1
                
                # If not, then add it to the dictionary.
                else:
                    mac_dictionary[match[5]] = 1

    # Prints out the items in the dcitionary.
    for item in list(mac_dictionary):
        print(item)
    # Prints out the count of how many unique iPhone devices in total there are in the file.
    print('Count = ', len(list(mac_dictionary)), '.', sep = '')
    
    return


def main():
    return

if '__name__' == 'main':
    main()


scan_file('dhcpdsmall.log')
