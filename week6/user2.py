#!/usr/bin/python3

# Ike-05182021: Initial version.

'''
Small script for populating a .csv file with relevant user information from the console.
Includes some basic handlers for ensuring proper input.
'''

# Import necessary modules.
import csv
import sys

# Regulated values in the form of dictionaries.

district_dictionary = {
    'IHS',
    'NJH',
    'AH',
    'BHS',
    'BC'
}

rank_dictionary = {
    'staff',
    'student'
}


def usage():
    '''Method to remind the user how to use the program with expected results.'''
    print(
        'Please use this program in the following way:\n'
        './user1.py\n'
        'without any additional arguments or switches. The\n'
        'program will take you through the user creation process.\n'
        'On completion, please look for the\n'
        'profile.csv\n'
        'file that contains your generated users.'
    )
    return

def get_string_input(phrase, expected_dictionary):
    '''Ensure that given value exists within a dictionary.'''
    while(True):
        # Get the user-provided input.
        value = input(phrase)
        # Check if value exists within the dictionary.
        if value in expected_dictionary:
            return value
        # If it does not, remind the user what values are permitted.
        else:
            print(
                'The permitted values for this section are:',
            )
            for item in expected_dictionary:
                print(
                    '\t',
                    item
                )
    return

def generate_user_id(profile):
    ''' Generates the userid for the profile.'''
    # Open the existing records.
    with open('profile.csv', 'r', newline = '') as file:
        reader = csv.reader(file)
        
        # Determine if the profile is a staff or a student.
        if profile[3] == 'staff':
            # Generate a starting profile base (firstnamelastname)
            user_id = (profile[0][0] + profile[1]).lower()
            # Iterate through the records.
            for item in reader:
                # Begin a loop while two records match.
                while(user_id == item[4]):
            
                    # Attempt to grab the last number of the account to increment.
                    try:
                        # Grab the last character of the existing user_id and then add one.
                        user_id = user_id[:-1] + str(int(user_id[-1:]) + 1)
                    
                    except:
                        # The last character is not a number, we append a 2 so that there is now a number.
                        user_id = user_id + '2'
                    
                    break
        # If not a staff account, then it must be a student account.
        else:
            # Generate the base userid.
            user_id = (profile[1][:5] + profile[0][:3]).lower()
            count = 0
            
            # Iterate through the records.
            for item in reader: 
                # Determine if the records match.
                if user_id == item[4][:8]:
                    # If they do, increment the number of the userid by one.
                    count = count + 1 
            # Append the number to the userid.
            user_id = user_id + '{0:02d}'.format(count)
            
    # Pass the userid back to the calling function.
    return user_id


def write_to_file(patient):
    '''Write given profile to .csv file.'''
    with open('profile.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(patient)
    return

def main():
    '''Basic structure of program: new file, instantiate profile-writing loop.'''
    with open('profile.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['firstname', 'lastname', 'school', 'accounttype', 'userid'])
    loop = 'y'
    # Begin loop for adding individuals.
    while(loop == 'y'):
        build_profile()
        loop = input('Would you like to add another individual (y/*)?\t')
    return

def build_profile():
    '''Constructs a profile to be written to a .csv format.'''
    profile = []
    # Get the type of account for the individual.
    profile.append(
        get_string_input(
            'Please enter the type of account for this individual (i.e. student or staff): ',
            rank_dictionary
        )
    )
    # Get the district associated with the individual.
    profile.append(
        get_string_input(
            'Please enter the district associated with the individual (press Enter for a complete list): ',
            district_dictionary
        )
    )
    # Get the last name of the individual. 
    name = input(
            'Please enter the last name of the individual: '
        )
    name.strip()
    name.lower()
    profile.append(name)

    # Get the first name of the individual.
    name = input(
            'Please enter the first name of the individual: '
        )
    name.strip()
    name.lower()
    profile.append(name)


    # Reverses profile list so that it fits required convention of
    # firstname, lastname, district, rank
    profile.reverse()
    # Generate the userid for the individual.
    profile.append(generate_user_id(profile))

    # Write the profile to the record file.
    write_to_file(profile)
    return


if '__name__' == 'main':
    main()

# Ensure that no additional arguments are intentionally passed
# when calling the program.
if len(sys.argv) == 1:
    main()
else:
    usage()
