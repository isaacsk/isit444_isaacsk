#!/usr/bin/python3

# Ike-20210523: Initial version.

''' Simple script for determining some basic details of an operating system the code is executed on and then sending that information
    to a database.
'''

# Import necessary modules.
import platform
import psutil
import netifaces
import pymysql

# Gather basic information as to the operating system specifics.
host_details = platform.uname()
host_name = platform.uname()[1]
cpu_count = psutil.cpu_count()
ram_gb = round(psutil.virtual_memory().total / 1000000000)
os_type = platform.system()
os_version = platform.uname()[2]
disks_count = round(len(psutil.disk_partitions()) / 8)
nic_count = len(netifaces.interfaces())
mac_address = netifaces.ifaddresses('eth0')[17][0]['addr']
ip_address = netifaces.ifaddresses('eth0')[2][0]['addr']

# List holding all above values.
all_items = [host_name, mac_address, ip_address, cpu_count, disks_count, ram_gb, os_type, os_version]

# Parameters for database
# host: localhost
# user: root
# password: Password1
# database: cmdb


def main():
    ''' Main method to determine details of os and write to database.'''
    db = pymysql.connect(
            host='localhost',
            user='root',
            password='Password1',
            db='cmdb',
            charset='utf8mb4'
        )
    # Initialize a cursor object to execute database queries.
    cursor = db.cursor()
    statement = 'INSERT INTO device VALUES (' + str(all_items)[1:-1] + ');'
    # Attempt to query the database and make the changes.
    try:
        print(cursor.execute(statement), 'lines affected.')
        db.commit()
    # The attempt failed, ensure no changes and let the user know.
    except:
        db.rollback()
        print('Failed.')
    # Close the connection.
    db.close()



if __name__ == '__main__':
    main()
