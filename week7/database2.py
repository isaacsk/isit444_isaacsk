#!/usr/bin/python3

# Ike-20210522: Initial version.

# Import necessary modules.
import pymysql
import sys
import csv
import json
import collections

# Parameters are as follows
# host: localhost
# user: root
# password: Password1
# database: cmdb

def main():
    ''' Short script for reading from a database and populating a respective .json or .csv file.
    with relevant information about the device(s) stored on that server.
    '''
    # Establish connection.
    db = pymysql.connect(
            host='localhost',
            user='root',
            password='Password1',
            db='cmdb',
            charset='utf8mb4'
        )
    # Set up a cursor object.
    cursor = db.cursor()
    # Send a command via the cursor to send information on the device table.
    cursor.execute('DESCRIBE device;')
    # Get all rows from the query.
    data = cursor.fetchall()
    # Initialize a list for storing the data from the query.
    heading_list = []
    # Put the headers in the header list.
    for item in data:
        heading_list.append(item[0])
    # Get the rest of the row information from the device table.
    cursor.execute('SELECT * FROM device;')
    data = cursor.fetchall()
    # Initialize row list to store information.
    item_list = []
    # Puts items in list.
    for item in data:
        item_list.append(item)
        print(item)
    # Determines if the user called a .csv.
    if sys.argv[1] == 'csv':
        # If they do, output is put into .csv format and written normally.
        with open('records.csv', 'w') as file:
            writer = csv.writer(file)
            writer.writerow(heading_list)
            for item in item_list:
                writer.writerow(item)
    # Check to see if user requested .json.
    elif sys.argv[1] == 'json':
        json_combined = collections.defaultdict(dict)
        for item in item_list:
            for i in item[1:]:
                # Combines objects so the resulting json looks nice.
                json_combined[item[0]][heading_list[item.index(i)]] = i
        
        with open('records.json', 'w') as file:
            # Write to file in json-friendly way.
            json.dump(json_combined, file)
    elif len(sys.argv < 2):
        print('Please try with csv or json e.g.'
              '\n./database2.py csv'
              '\n./database2.py json'
        )
    # If neither .csv or .json specified, remind user how to use program.
    else:
        print('Try with csv or json e.g.'
             '\n./database2.py csv'
            '\n./database2.py json'
        )
    # Close connection
    db.close()

    return

if __name__ == '__main__':
    main()


