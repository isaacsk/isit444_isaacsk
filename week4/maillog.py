#! /usr/bin/env python
# Ike-20210511: Initial version.

# Short script for intaking a text file, parsing it for SMTP traffic, and then
# stripping out the FQDN and IPv4 addresses for servers.


# Import the regex module for python to execute CPU-intensive searches.
import re

# Import the system module to process commands from the Shell.
import sys

# Import the csv module to write to a .csv file.
import csv

# Import the os module for python to help establish file paths.
import os


def usage():
    """Displays use case for script."""
    print(
            'The appropriate usage for this command is:\n'
            'maillog.py filename\n'
            'e.g.\n'
            'maillog.py mail.log\n'
            'The output is stored in a .csv file titled\n'
            '\"servers.csv\"\n'
            'please ensure the file is located in the same\n'
            'folder that you are executing the command within.'
    )


def main():
    """Takes a filename, strips out the FQDN and IPv4 addresses, and writes them to a .csv file."""

    # Check to ensure that:
    # * There are only two values entered for the script.
    # & There is a valid pathway to the filename entered.
    # & The file is a file and not a directory.
    if (
        len(sys.argv) == 2
        and os.path.exists(str(sys.argv[1]))
        and os.path.isfile(str(sys.argv[1]))
    ):
    
        # Try getting the full absolute path for the file.
        try:
            filename = os.path.abspath(str(sys.argv[1]))

        # If there is no absolute path for the file, throws exception.
        except:
                                                              
            # Remind the user how to use the program.
            usage()

        # With a valid path, 
        with open(filename, 'r') as file:
                                                                                        
            # Initialize a registry to store the values in as a list.
            registry = []
                                                                                                    
            # Begin iterating through the given file.
            for line in file:

                # Regular expression query, looks for the 'nt=' phrase 
                # to start, and then scoops out everything after that 
                # until the closing bracket ']'.
                phrase = re.search('nt=[^\[]+[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}]', line)

                # If there is a regex match, executes the following.
                if phrase:
                                                                                                                                                                                  
                    # Takes the matching line, splits it at the '=' sign.
                    match = str(phrase).split('=')

                    # Takes the final section, containing the
                    # FQDN and IPv4 addresses and splits them in two.
                    match = match[3].split('[')

                                                                                                                                                                                                                    # Remove the last three characters from the match.
                    match[1] = match[1][:-3]

                    # If the matching portion does NOT already
                    # exist within the registry, add it.
                    if not match in registry:
                        registry.append(match)

        # Open the file for writing. Creates the file if it does not already exist.
        # Overwrites the file if it does exist.
        with open('servers.csv','w') as file:

            # Initialize the writer for the file.
            writer = csv.writer(file)

            # Write the headers for the file at the top.
            writer.writerow(['Server FQDN','Server IPv4'])

                                                                                                                                                                                                            # Write the contents of the registry to the .csv file.
            writer.writerows(registry)

                                                                                                                                                                                                    # If the input does not match the expected value.
    else:
                                                                                                                                                                                                        # Remind the user how to use the program.
        usage()

    return


if __name__ == '__main__':
    
    main()
