#! /usr/bin/env python
# Ike-20210511: Initial version.

# Short script for taking a number of inputs and returning their average.

# Import the system module to process commands from the Shell.

import sys

def usage():
    """Displays use case for script."""
    print('Please enter three whole, real numbers to have their average computed.\n'
          'For example:\n'
          'python3 avg3.py 1 2 3')
    exit()


def main():
    """Standard use case for the program, averages three numbers that are entered into the program."""
                        
    # Checks to make sure the right number of arguments were entered.
    if len(sys.argv) == 4:
                                    
        # Remove the first value from the entries.
        # This is usually the name of the function (avg3.py).
        sys.argv.pop(0)
                                                
        # Compiles a list of the remaining values.
        values = sys.argv
                                                            
        # Initializes a list to store the values within.
        numberlist = []
        
        # Try to run the program normally.
        try:

            # Begin iterating through the list of values.
            for number in values:
                                                                                                        
                # Attempt an integer type of the iterator value.
                int(number)

                # If it is possible, attempt to append it to the value list.
                numberlist.append(int(number))

        # If an exception is thrown (non-iterables, non-integers),
        # remind the user how to use the program.  
        except:
            usage()

    # If the list of values entered is not of length 3,
    # remind the user how to use the program.      
    else:
        usage()

    # Calculate the mean average of the 3 values entered.
    # This is guaranteed to not be a division by zero.
    average = sum(numberlist) / len(numberlist)

    # Lets the user know the average of their submitted numbers.
    print('The average of', values[0] + ',', values[1] + ', and', values[2], 'is {0:.2f}.'.format(average))

    return


if __name__ == '__main__':
                                                                                                                                                                                            main()
