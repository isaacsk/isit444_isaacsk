#!/usr/bin/python3

# Ike-20210511

''' Simple script for pinging the addresses listed in a given file. '''

# Import necessary modules.
import os
import platform
import sys
import subprocess
import re


def usage():
    """Displays use case for script."""
    print(
            'The appropriate usage for this command is:\n'
            'ping1.py filename\n'
            'e.g.\n'
            'ping1.py pingfile.txt\n'
            'The output is printed to the console.\n'
            'Please ensure the file is located in the same\n'
            'folder that you are executing the command within.'
        )
    return


def probe(address, parameter):
    '''Method for checking the address and returning the average return speed of the packet. If no packet is echoed in response, it is indicated as NotFound.'''
    try:
        command = subprocess.Popen(['ping', address, parameter, '2', '-w2'], stdout = subprocess.PIPE, stderr = subprocess.PIPE) 
        # Wait for command to complete its execution.
        command.wait()
        # Receive the output from the executed command.
        output = command.communicate()
        # Regex search phrase (selecting the average time for two packets).
        search = '\/[0-9].[0-9]{1,11}'
        # Execute the regex on the output so we do not display any unnecessary information.
        output = re.findall(search, output[0].decode())[0]
        # Remove the / at the beginning of the search phrase.
        output = str(output)[1:]
        return output
    # Case for if the query is malformed, or the ping is not successful for any reason.
    except:
        return 'NotFound'

# Check to ensure that:
# * There are only two values entered for the script.
# & There is a valid pathway to the filename entered.
# & The file is a file and not a directory.
if (
    len(sys.argv) == 2
    and os.path.exists(str(sys.argv[1]))
    and os.path.isfile(str(sys.argv[1]))
    ):
        
        current_os = platform.system().lower()
        if current_os == "windows":
            parameter = "-n"
        else:
            parameter = "-c"

        # Try getting the full absolute path for the file.
        try:
            filename = os.path.abspath(str(sys.argv[1]))

        # If there is no absolute path for the file, throws exception.
        except:
                                  
            # Remind the user how to use the program.
            usage()
   
        with open(filename, 'r') as file:
            print('Address, TimeToPing (ms)')
            
            for line in file:

                line = line.rstrip()
                address = str(line)
                print(line, probe(address, parameter), sep = ', ')

else:
    usage()
