#!/usr/bin/python3

# Ike-20210514: Initial version.

# Import necessary modules.
import platform
import psutil
import netifaces
import sys
import csv


host_details = platform.uname()
host_name = ['HostName', platform.uname()[1]]
cpu_count = ['CPU (count)', psutil.cpu_count()]
ram_gb = ['RAM (GB)', round(psutil.virtual_memory().total / 1000000000)]
os_type = ['OSType', platform.system()]
os_version = ['OSVersion', platform.uname()[2]]
disks_count = ['Disks (Count)', round(len(psutil.disk_partitions()) / 8)]
nic_count = ['NIC (Count)', len(netifaces.interfaces())]
# List of all items for use in the methods below.
all_items = [host_name, cpu_count, ram_gb, os_type, os_version, disks_count, nic_count]

def usage():
    ''' Reminds the user how to use the program correctly. '''
    print('There are 3 available commands for this program:\n'
            'screen\n'
            'csv\n'
            'json\n'
            'e.g.\n'
            'serverinfo2.py screen'
    )

    return


def list_NICs_screen():
    ''' Writes the information of the Network Interface Cards to the console. '''
    for NIC in netifaces.interfaces():
        print('\t' + NIC, netifaces.ifaddresses(NIC)[2][0]['addr'], sep = ' = ')
    return

def display_screen():
    ''' Writes the information to the console. '''
    for item in all_items:
        print(item[0], item[1], sep = ' = ')
    print('NIC Names/IPs:')
    list_NICs_screen()

def display_csv():
    ''' Writes the information to a .csv format, with a header line. '''
    with open('serverinfo.csv', 'w') as file:

        writer = csv.writer(file)
        all_items_header = []
        all_items_entry = []
        # Iterate through the list of items, and builds the two rows of information.
        for item in all_items:
            all_items_header.append(item[0])
            all_items_entry.append(item[1])
        # Iterate through the available Network Interface Cards and builds the two rows of information.
        for NIC in netifaces.interfaces():
            all_items_header.append(NIC)
            all_items_entry.append(netifaces.ifaddresses(NIC)[2][0]['addr'])
        # Writes the information in two rows to the .csv file.
        writer.writerow(all_items_header)
        writer.writerow(all_items_entry)
    return

def display_json():
    ''' This method is very messy. Good candidate for re-writing. '''
    with open('serverinfo.json', 'w') as file:
        file.write('{')
        # Writes the items in object format to the file.
        for item in all_items:
            file.write('\n\t\"' + str(item[0]) + '\": \"' + str(item[1]) + '\",')
        # Writes the last object, omitting the comma.
        file.write('\n\t\"NIC Names/IPs\": [\n\t\t{')
        # Writes the Network Interface Cards to the file in nested object format.
        for NIC in netifaces.interfaces()[:-1]:
            file.write('\n\t\t\t\"' + NIC + '\": \"' + netifaces.ifaddresses(NIC)[2][0]['addr'] + '\",')
        # Writes the last Network Interface Card to the file in nested object format, omitting the comma.
        for NIC in netifaces.interfaces()[-1:]: 
            file.write('\n\t\t\t\"' + NIC + '\": \"' + netifaces.ifaddresses(NIC)[2][0]['addr'] + '\"')

        file.write('\n\t\t}\n\t]')
        # These lines intentionally spaced out in the event that more items are added in the future.
        file.write('\n}')
    return

# Determine which method to display the information.
if len(sys.argv) == 2:
    if sys.argv[1] == 'screen':
        display_screen()
    elif sys.argv[1] == 'csv':
        display_csv()
    elif sys.argv[1] == 'json':
        display_json()
    else:
        usage()
else:
    usage()




if '__name__' == '__main__':
    main()
