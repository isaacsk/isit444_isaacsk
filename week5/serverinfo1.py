#!/usr/bin/python3

# Ike-20210513: Initial version.

''' Simple script for determining some basic details of an operating system the code is executed on. '''

# Import necessary modules.
import platform
import psutil
import netifaces

# Gather basic information as to the operating system specifics.
host_details = platform.uname()
host_name = platform.uname()[1]
cpu_count = psutil.cpu_count()
ram_gb = round(psutil.virtual_memory().total / 1000000000)
os_type = platform.system()
os_version = platform.uname()[2]
# This might need modification. It attempts to determine the number of drives present on the host system.
disks_count = round(len(psutil.disk_partitions()) / 8)
nic_count = len(netifaces.interfaces())



def list_NICs():
    ''' Lists the Network Interface Cards and their associated addresses.'''
    for NIC in netifaces.interfaces():
        print('\t' + NIC, netifaces.ifaddresses(NIC)[2][0]['addr'], sep = ' = ')
    return

# Prints the output to the console.
print('HostName =', host_name)
print('CPU (count) =', cpu_count)
print('RAM (GB) =', ram_gb)
print('OSType =', os_type)
print('OSVersion =', os_version)
print('Disks (Count) =', disks_count)
print('NIC (Count) =', nic_count)
print('NIC Names/IPs:')
list_NICs()




if '__name__' == '__main__':
    main()
