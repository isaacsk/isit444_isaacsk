#!/usr/bin/python3
#Ike-20210420: Initial version.
#
#
#
def main():
    """Requests the name of the user, their preferred color, \
    and then prints them to the console."""
	
    name = input('What is your preferred user name?\n')
    color = input('What is your favorite color?\n')

    print('The favorite color for', name, 'is', color + '.')

if __name__ == '__main__':
    main()
