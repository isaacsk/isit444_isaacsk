#!/usr/bin/python3

# Ike-20210421: Revision, brought into compliance with ScriptTemplate.py
#               Added get_int_input method.

# Grade Calculator - A program that will average 3 numeric exam grades,
# return an average test score, a corresponding letter grade, and a message
# stating whether the student is passing. 

# 90+ = A, 80 - 89 =B, 70 - 79 = C 60 - 69 = D, < 59 = F

def get_int_input(phrase):
    """ Given a string, requests an integer value from the user.
        Provides a guarantee on the output being an integer
        value between 0 and 200 inclusive.
    """
    input_warning = ('Please enter a real, whole number' 
                    ' (between 0 and 200).')
    while(True):
        try:  
            number = int(input(phrase))
            # Supply limits for numeric guarantee.
            if(
                number >= 0 and 
                number <= 200
                ):
                break                                  
            else:
                print(input_warning)

        # This is the only exception worth noting because all other
        #  data types except for integer will throw this exception.
        except ValueError:
            print (input_warning)

    return number


#   Deprecated from python 1.4. Preserved here for posterity.
#def avg(grades):
#    # Get average grade
#    mysum = 0
#    for exam in grades:
#        mysum = mysum + exam
#    return(mysum / len(grade))

def grade_letter_conversion(avg):
    ''' Convert grade to letter grade.
    '''
    if avg >= 90:
        letter = 'A'
    elif avg >= 80 and avg < 90:
        letter = 'B'
    elif avg >= 70 and avg < 80:
        letter = 'C'
    elif avg >= 60 and avg < 70:
        letter = 'D'
    else:
        letter = 'F'
    return(letter)

# Print out the grade
def print_grade(grades, number_grade, letter_grade):
    print(' ')
    index = 1
    for grade in grades:
        print('Exam', index, ':', str(grade))
        index = index + 1
    print('Average: ' + str(number_grade))
    print('Grade: ' + letter_grade)

    if letter_grade == 'F':
        print('Student is failing.')
    else:
        print ('Student is passing.')

def main():
    ''' Request exam scores from user and place into a list.
    '''
    exam1 = get_int_input('Please type and enter exam grade one:    ')
    exam2 = get_int_input('Please type and enter exam grade two:    ')
    exam3 = get_int_input('Please type and enter exam grade three:  ')
    grades = [exam1, exam2, exam3]

    # Call functions to do the work and print output
    grade_average = sum(grades) / len(grades)
    letter_grade = grade_letter_conversion(grade_average)
    print_grade(grades, grade_average, letter_grade)

# Run main() if script called directly
if __name__ == '__main__':
        main()
